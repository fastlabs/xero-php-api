<center>

<?php
$certs = dirname(__DIR__).'/certs/';
if (isset($_POST['countryName'])) {
	
	$privkey = openssl_pkey_new( [ 'private_key_bits' => 1024 ]);
	$csr = @openssl_csr_new($_POST, $privkey);
	if ($csr!='') {
		
		$sscert = openssl_csr_sign($csr, null, $privkey, 1825);
		openssl_x509_export($sscert, $publickey);
		openssl_pkey_export($privkey, $privatekey);
		openssl_csr_export($csr, $csrStr);
		
		file_put_contents($certs.'privatekey.pem', $privatekey);
		file_put_contents($certs.'publickey.pem', $publickey);
		file_put_contents($certs.'publickey.crt', $csrStr);
		
		?>
		<H2>Keys generated</H2>
		<div style="width:500px;text-align:left;">
		Private key: <?=$certs?><span style="color:green" />privatekey.pem</span><br />
		Public key: <?=$certs?><span style="color:green" />publickey.pem</span><br />
		Certificate: <?=$certs?><span style="color:green" />publickey.crt</span><br />
		</div>
		<br />
		<br />
		You can upload the file <span style="color:green" />certs/publickey.pem</span> or copy/paste it from here<br />
		<pre id="myInput"><?=$publickey?></pre>
		<br />
		<button id="copyButton">Copy to clipboard</button>
		<br /><br /><br />
		Now you can create the app on Xero like:<br />
		<img src="private-app-certs.png" style="height:800px;"/>
		</center>
		
<script>
function copyFunction() {
  const copyText = document.getElementById("myInput").textContent;
  const textArea = document.createElement('textarea');
  textArea.textContent = copyText;
  document.body.append(textArea);
  textArea.select();
  document.execCommand("copy");
  textArea.parentNode.removeChild(textArea);
  
}

document.getElementById('copyButton').addEventListener('click', copyFunction);
</script>
		<?php
		exit;
	} else {
		?>
		<h1 style="color:red">Please specify all fields.</h1>
		<?php
	}
}

?>

<H1>Keyword generation</H1>

<form method="POST" action="?">
<table>
	<tr><td>Country Code</td><td><input name="countryName" type="text" placeHolder="eg:AU" size="6" /></td></tr>
	<tr><td>State / Province</td><td><input name="stateOrProvinceName" type="text" placeHolder="eg: New South Wales" size="20" /></td></tr>
	<tr><td>Locality</td><td><input name="localityName" type="text" placeHolder="eg: Sydney" size="20" /></td></tr>
	<tr><td>Organization</td><td><input name="organizationName" type="text" placeHolder="eg: Fast Labs" size="20" /></td></tr>
	<tr><td>Name</td><td><input name="organizationalUnitName" type="text" placeHolder="eg: My Xero Private App" size="15" /></td></tr>
	<tr><td>Version</td><td><input name="commonName" type="text" placeHolder="eg: test app"" size="10" /></td></tr>
	<tr><td>Email Address</td><td><input name="emailAddress" type="text" placeHolder="eg: email@gmail.com" size="20" /></td></tr>
	<tr>
		<td colspan="2" style="text-align:center">
			<?php
			if (!is_writable($certs)) {
				print('<span style="color:red">'.$certs.' is not writable<br /></span>');
				print('<input type="submit" value="GENERATE CERTIFICATES" disabled />');
			} elseif (file_exists($certs.'privatekey.pem') || file_exists($certs.'publickey.pem')) {
				print('<span style="color:red">Certificates already exists!<br />Delete them to create new ones...<br ./></span>');
				print('<input type="submit" value="GENERATE CERTIFICATES" disabled />');
			} else {
				print('<input type="submit" value="GENERATE CERTIFICATES" />');
			}
			?>
			
		</td>
	</tr>
</table>
</form>
</center>