<?php

set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . PATH_SEPARATOR);

spl_autoload_register(function($class) {
	
	if (class_exists($class)) return;
	
	$classFileName = str_replace('\\', '/', $class);
	if (stream_resolve_include_path(__DIR__.'/../src/'.$classFileName . '.php')) require_once(__DIR__.'/../src/'.$classFileName . '.php');
	return false;
});

require_once(__DIR__ . '/../conf.php');

// first try to use private app
try {
	$xeroApp = \FastLabs\Xero\Application::Private();
} catch (\Exception $e) {
	try {
		$xeroApp = \FastLabs\Xero\Application::Public();
	} catch (\Exception $e) {
		print($e->getMessage());
		die ();
	}
}

if ($xeroApp::$TYPE=='Public') {
	if (isset($_REQUEST['wipe'])) {
		session_destroy();
		header("Location: /");
		exit;
	}
	
	if (!$xeroApp->isLogged()) {
		
		if (isset($_REQUEST['oauth_token'])) {
			$xeroApp->checkCallBack();
		}
		
		if (isset($_REQUEST['authenticate'])) {
			header("Location: ".$xeroApp->getLoginUrl());
			exit;
		}
		
		echo '<center><a href="?authenticate=1">Click here to access via Xero Public App</a></center>';
		exit;
	}
	
	$org = $xeroApp->loadOrganization();
	$o = $org->getOne();
	if (!$o) {
		if ($errors = $org->getErrors()) {
			if (array_key_exists(401, $errors)) {
				// session expired!
				session_destroy();
				header("Location: /");
				exit;
			}
		}
	}
	print('<div class="topBar allGood"><a href="?wipe=1">Disconnect</a></div>');
} else {
	$org = $xeroApp->loadOrganization();
	$o = $org->getOne();
	if (!$o) {
		print('<div class="topBar allGood">Something is wrong with the Private app.</div>');
		exit;
	}
	print('<div class="topBar allGood">Using Private app, no need to authenticate</div>');
}

print(" {$o->Name} - {$o->OrganisationID}");

?>