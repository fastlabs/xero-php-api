**XERO CERTIFICATES**

For Private Applications you need to create certificates and configure them in  your Xero app in oredr to make it work.

---

## Private Application Setup

### Generate certificate files

In console (terminal) for Linux or MacOs:

```sh
openssl genrsa -out privatekey.pem 1024
openssl req -new -x509 -key privatekey.pem -out publickey.cer -days 1825
openssl pkcs12 -export -out public_privatekey.pfx -inkey privatekey.pem -in publickey.cer
```


Now you can create your app in Xero:

 1. Go to Xero https://www.xero.com/ and register for free account
 2. Go to https://developer.xero.com/myapps/ and create a PRIVATE APP
 3. Upload the .cer file you just created