**XERO PHP API**

Simple / Basic PHP library to interact with Xero billing platform

---

## Installation


### composer

In order to install this library via composer run the following command in the console:

```sh
composer require fastlabs/xero
```

or add the package manually to your composer.json file in the require section:

```json
"fastlabs/xero": "^2.1"
```

Example basic code:

```php
<?php

require_once('vendor/autoload.php');

\FastLabs\Xero\Application::$XeroConsumerKey  = 'your consumer key';
\FastLabs\Xero\Application::$XeroSharedSecret = 'your shared secret';
\FastLabs\Xero\Application::$RSAprivateKey    = 'certs/privatekey.pem';
\FastLabs\Xero\Application::$RSApublicKey     = 'certs/publickey.cer';

$xeroApp = FastLabs\Xero\Application::Private();

$org = $xeroApp->loadOrganization()->getOne();
print "Access granted to Xero for Organizatio {$o->OrganisationID}";
```

### manual

```sh
# git clone https://bitbucket.org/fastlabs/xero-php-api.git
# cd xero-php-api.git
```

generate certificate files

```sh
cd certs
openssl genrsa -out privatekey.pem 1024
openssl req -new -x509 -key privatekey.pem -out publickey.cer -days 1825
openssl pkcs12 -export -out public_privatekey.pfx -inkey privatekey.pem -in publickey.cer
```

Now you can create your Private app in Xero:

 1. Go to Xero https://www.xero.com/ and register for free account
 2. Go to https://developer.xero.com/myapps/ and create a PRIVATE APP
 3. Upload the .cer file you just created


Now you can configure the application

```sh
cd ..
vi conf.php
```

You conf.php file should be like:

```php
<?php
/* configuration for private app */
\FastLabs\Xero\Application::$XeroConsumerKey  = 'PRIVATE_APPLICATION__CONSUMER_KEY';
\FastLabs\Xero\Application::$XeroSharedSecret = 'PRIVATE_APPLICATION__CONSUMER_SECRET';
\FastLabs\Xero\Application::$RSAprivateKey    = __DIR__.'/certs/privatekey.pem';
\FastLabs\Xero\Application::$RSApublicKey     = __DIR__.'/certs/publickey.cer';


/* configuration for public app */
// \FastLabs\Xero\Application::$XeroConsumerKey  = 'PUBLIC_APPLICATION__CONSUMER_KEY';
// \FastLabs\Xero\Application::$XeroSharedSecret = 'PUBLIC_APPLICATION__CONSUMER_SECRET';
// \FastLabs\Xero\Application::$OauthCallBack    = 'http://127.0.0.1:8000/?callback';
```

And now, let's give it a go...

```sh
# cd public
# php -S localhost:8000
```

Open http://localhost:8000 on your browser

---

## Private Application initialization

```php
<?php

\FastLabs\Xero\Application::$XeroConsumerKey  = 'PRIVATE_APPLICATION__CONSUMER_KEY';
\FastLabs\Xero\Application::$XeroSharedSecret = 'PRIVATE_APPLICATION__CONSUMER_SECRET';
\FastLabs\Xero\Application::$RSAprivateKey    = __DIR__.'/certs/privatekey.pem';
\FastLabs\Xero\Application::$RSApublicKey     = __DIR__.'/certs/publickey.cer';

$xeroApp = \FastLabs\Xero\Application::Private();

// check organization name (also double check if login is valid)
$org = $xeroApp->loadOrganization();
$o = $org->getOne();
if (!$o) {
	if ($errors = $org->getErrors()) {
		if (array_key_exists(401, $errors)) {
			echo 'You are not authorized?! please check your configuration';
			exit;
		}
	}
}

echo "Access via Public application for organization {$o->Name} ( OrganisationID: {$o->OrganisationID} )";
print(" {$o->Name} - {$o->OrganisationID}");
```

## Public application initialiazion & session login-logout example
```
<?php

// \FastLabs\Xero\Application::$XeroConsumerKey  = 'PUBLIC_APPLICATION__CONSUMER_KEY';
// \FastLabs\Xero\Application::$XeroSharedSecret = 'PUBLIC_APPLICATION__CONSUMER_SECRET';
// \FastLabs\Xero\Application::$OauthCallBack    = 'http://127.0.0.1:8000/?callback';

$xeroApp = \FastLabs\Xero\Application::Public();

// logout function
if (isset($_REQUEST['wipe'])) {
	session_destroy();
	header("Location: /");
	exit;
}

// check if already logged-in
if (!$xeroApp->isLogged()) {
	
	// check if is a callBack from xero (oAuth process step 2)
	if (isset($_REQUEST['oauth_token']))
		$xeroApp->checkCallBack();
	
	if (isset($_REQUEST['authenticate'])) {
		echo '<a href="' .$xeroApp->getLoginUrl().'">Access via Xero</a>';
	} else {
		echo '<a href="?authenticate=1">Start the login process?</a>';
	}
	exit;
}

// check organization name (also double check if login is still valid or expired)
$org = $xeroApp->loadOrganization();
$o = $org->getOne();
if (!$o) {
	if ($errors = $org->getErrors()) {
		if (array_key_exists(401, $errors)) {
			print('Session expired... <a href="?wipe=1">Reconnect</a>');
			exit;
		}
	}
}

echo "Access via Public application for organization {$o->Name} ( OrganisationID: {$o->OrganisationID} )";
print(" {$o->Name} - {$o->OrganisationID}");
```