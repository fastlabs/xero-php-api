<?php

namespace FastLabs\Xero;

class StructuresLoader extends XeroOAuth {
	
	/**
	 * @return \FastLabs\Xero\Structures\Organization
	 */
	public function loadOrganization() {
		return new \FastLabs\Xero\Structures\Organization($this);
	}
	
	/**
	 * @return \FastLabs\Xero\Structures\Contact
	 */
	public function loadContact() {
		return new \FastLabs\Xero\Structures\Contact($this);
	}
	
	/**
	 * @return \FastLabs\Xero\Structures\Invoice
	 */
	public function loadInvoice() {
		return new \FastLabs\Xero\Structures\Invoice($this);
	}
	
	/**
	 * @return \FastLabs\Xero\Structures\Payment
	 */
	public function loadPayment() {
		return new \FastLabs\Xero\Structures\Payment($this);
	}
	
	/**
	 * @return \FastLabs\Xero\Structures\ItemCode
	 */
	public function loadItemCodes() {
		return new \FastLabs\Xero\Structures\ItemCode($this);
	}
}
