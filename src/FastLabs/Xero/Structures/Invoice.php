<?php 

namespace FastLabs\Xero\Structures;

Class Invoice extends Skeleton {
	
	const STATUS_DRAFT = 'DRAFT';
	const STATUS_SUBMITTED = 'SUBMITTED';
	const STATUS_DELETED = 'DELETED';
	const STATUS_AUTHORISED = 'AUTHORISED';
	const STATUS_PAID = 'PAID';
	const STATUS_VOIDED = 'VOIDED';
	
	/** @var string ACCREC | ACCPAY */
	public $Type;
	
	public $InvoiceID;
	public $InvoiceNumber;
	public $Reference;
	
	/** @var array */
	public $Payments;
	
	/** @var array */
	public $CreditNotes;
	
	/** @var array */
	public $Prepayments;
	
	/** @var array */
	public $Overpayments;
	
	/** @var float */ 
	public $AmountDue;
	
	/** @var float */
	public $AmountPaid;
	
	/** @var float */
	public $AmountCredited;
	
	/** @var bool */
	public $SentToContact;
	
	/** @var float */
	public $CurrencyRate;
	
	/** @var bool */
	public $HasErrors;
	
	/** @var bool */
	public $IsDiscounted;
	
	/** @var bool */
	public $HasAttachments;
	
	/** @var Contact */
	public $Contact;
	
	/** @var string 2018-11-13T00:00:00 */
	public $DateString;
	
	/** @var string /Date(1542067200000+0000)/ */
	public $Date;
	
	/** @var string 2018-11-13T00:00:00 */
	public $DueDateString;
	
	/** @var string /Date(1543536000000+0000)/ */
	public $DueDate;
	
	/** @var string uuid */
	public $BrandingThemeID;
	
	/** @var string */
	public $Status;
	
	/** @var string Exclusive */
	public $LineAmountTypes;
	
	/** @var InvoiceItem[] */
	public $LineItems;
	
	/** @var float */
	public $SubTotal;
	
	/** @var float */
	public $TotalTax;
	
	/** @var float */
	public $Total;
	
	/** @var string /Date(1543536000000+0000)/ */
	public $UpdatedDateUTC;
	
	/** @var string EUR */
	public $CurrencyCode;
	
	
	protected $_actionName = 'Invoices';
	protected $_GUID = 'InvoiceID';
}