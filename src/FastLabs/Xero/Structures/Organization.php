<?php 

/**
 * https://developer.xero.com/documentation/api/organisation
 */
namespace FastLabs\Xero\Structures;

Class Organization extends Skeleton {
	
	protected $_actionName = 'Organisations';
	protected $_GUID       = 'OrganisationID';
	
	public function __construct(&$xero) {
		$this->xero = $xero;
	}
	
	/** @var string UUID */
	public $OrganisationID;
	
	/** @var string */
	public $APIKey;
	
	/** @var string */
	public $Name;
	
	/** @var string */
	public $LegalName;
	
	/**  @var bool */
	public $PaysTax;
	
	/** @var string  AU | NZ */
	public $Version;
	
	/** @var string eg: COMPANY */
	public $OrganisationType;
	
	/** @var string eg: AUD */
	public $BaseCurrency;
	
	/** @var string eg: NZ */
	public $CountryCode;
	
	/** @var bool */
	public $IsDemoCompany;
	
	/** @var string eg: ACTIVE */
	public $OrganisationStatus;
	
	/** @var string */
	public $TaxNumber;
	
	/** @var int */
	public $FinancialYearEndDay;
	
	/** @var int */
	public $FinancialYearEndMonth;
	
	/** @var string eg: PAYMENTS */
	public $SalesTaxBasis;
	
	/** @var string eg: TWOMONTHS */
	public $SalesTaxPeriod;
	
	/** @var string eg: \Date(1254268800000+0000)\ */
	public $PeriodLockDate;
	
	/** @var string eg: \Date(1254268800000)\ */
	public $CreatedDateUTC;
	
	
	/** @var string eg: COMPANY */
	public $OrganisationEntityType;
	
	/** @var string eg: NEWZEALANDSTANDARDTIME */
	public $Timezone;
	
	/** @var string eg: !23eYt */
	public $ShortCode;
	
	/** @var string eg: BUSINESS */
	public $Edition;
	
	// TODO: stopped at $Edition need to process all fields
	/*
	 mancano da aggiungere
	  "Edition": "BUSINESS",
      "Class": "DEMO",
      "Addresses": [
        {
          "AddressType": "POBOX",
          "AddressLine1": "3 Market Place",
          "AddressLine2": "Twizel 7901",
          "City": "Twizel",
          "PostalCode": "7901",
          "Country": "New Zealand",
          "AttentionTo": "Bentley Rhythm Ace"
        }
      ],
      "ExternalLinks": [
        {
          "LinkType": "Facebook",
          "Url": "http://facebook.com/Xero.Accounting"
        },
        {
          "LinkType": "Twitter",
          "Url": "http://twitter.com/xeroapi"
        },
        {
          "LinkType": "GooglePlus",
          "Url": "https://plus.google.com/u/0/105727595143346068928/"
        },
        {
          "LinkType": "LinkedIn",
          "Url": "http://www.linkedin.com/company/xero"
        }
      ],
      "PaymentTerms": {
        "Bills": {
          "Day": 5,
          "Type": "OFCURRENTMONTH"
        },
        "Sales": {
          "Day": 20,
          "Type": "OFFOLLOWINGMONTH"
        }
      }
	 */
	
}