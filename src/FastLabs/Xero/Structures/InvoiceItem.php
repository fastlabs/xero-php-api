<?php 

namespace FastLabs\Xero\Structures;

Class InvoiceItem {
	
	public const LINE_AMOUNT_EXCLUSIVE = 'Exclusive';
	public const LINE_AMOUNT_INCLUSIVE = 'Inclusive';
	public const LINE_AMOUNT_NOTAX     = 'NoTax';
	
	public $ItemCode;
	
	public $Description;
	
	/** @var float */
	public $UnitAmount;
	
	/**
	 * https://developer.xero.com/documentation/api/types#TaxTypes
	 * @var string NONE | INPUT | INPUT 
	 */
	public $TaxType;
	
	/** @var float */
	public $TaxAmount;
	
	/** @var string */
	public $LineAmount;
	
	/** @var string */
	public $AccountCode;
	
	/** @var TrackingCategory[] */
	public $Tracking;
	
	/** @var float */
	public $Quantity;
	
	/** @var string uuid */
	public $LineItemID;
}