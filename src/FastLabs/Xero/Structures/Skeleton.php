<?php

namespace FastLabs\Xero\Structures;

class Skeleton {
	
	/** @var \FastLabs\Xero\Application */
	protected $xero;
	
	protected $_last = false;
	
	protected $_params = [];
	
	public function __construct(&$xero) {
		$this->xero = $xero;
	}
	
	public function where($var,$value) {
		if (!array_key_exists('where', $this->_params)) $this->_params['where'] = [];
		if ($value===true) {
			$this->_params['where'][] = $var.'=true';
		} elseif ($value===false) {
			$this->_params['where'][] = $var.'=false';
		} else {
			$this->_params['where'][] = $var.'=="'.$value.'"';
		}
		return $this;
	}
	public function inList($var,$mode) {
		$this->_params[$var] = $mode;
		return $this;
	}
	public function order($var,$mode='ASC') {
		$this->_params['order'] = $var.' '.$mode;
		return $this;
	}
	public function page($var) {
		$this->_params['page'] = (int)$var;
		return $this;
	}
	
	public function getErrors() {
		if (isset($this->_last->data->Status) && $this->_last->data->Status!='OK') return false;
		
		if (isset($this->_last->code) && $this->_last->code==401) {
			return ['401'=>'Not Authorize'];
		}
		if (isset($this->_last->data->Type) && $this->_last->data->Type=='ValidationException' && is_array($this->_last->data->Elements)) {
			foreach ($this->_last->data->Elements as $e) {
				return $e->ValidationErrors;
			}
		}
		return ['Unknown error'];
	}
	
	public function getLastDebug() {
		return $this->_last;
	}
	
	/**
	 *
	 * @return boolean|$this
	 */
	public function save() {
		$saveGUID = $this->{$this->_GUID};
		$check = $this->xero->postJson($this->xero->url($this->_actionName.'/'.$saveGUID, 'core'), $this);
		
		$this->_last = $check;
		if (!isset($check->data->Status) || $check->data->Status!='OK') return false;
		
		$name = $this->_actionName;
		return $this->loadFromWebReply( array_shift($check->data->$name) );
	}
	
	/**
	 *
	 * @param object $obj (from json_decode xero request)
	 * @return $this;
	 */
	public function loadFromWebReply($obj) {
		if (!$obj) return false;
		foreach ($obj as $k => $v) {
			if (property_exists($this, $k)) {
				// TODO: maybe we need to load new (sub) objects
				$this->$k = $v;
			}
		}
		return $this;
	}
	
	/**
	 * get contacts list
	 * @return false|$this|$this[]
	 */
	public function query($id=null) {
		$name = $this->_actionName;
		if ($this->_sendRequest($name.'/'.$id, 'core')) {
			if ($id!==null){
				return $this->loadFromWebReply(array_shift($this->_last->data->$name));
			} else {
				$out = [];
				$idCode = $this->_GUID;
				foreach ($this->_last->data->$name as $idx => $v) {
					
					$class = get_called_class();
					$tmp = new $class($this->xero);
					$tmp->loadFromWebReply($v);
					$out[$tmp->$idCode] = $tmp;
					unset($this->_last->data->$name[$idx]);
				}
				return $out;
			}
		} else {
			if ($id!==null) return false;
			return [];
		}
	}
	
	/**
	 * get single result
	 * @return false|$this
	 */
	public function getOne() {
		return $this->query(false);
	}
	
	protected function _sendRequest($section,$opt) {
		
		if (array_key_exists('where', $this->_params)) $this->_params['where'] = implode('&&',$this->_params['where']);
		
		$this->_last = $this->xero->getJson($this->xero->url($section, 'core'),$this->_params);
		
		$this->_params = [];
		
		if (!isset($this->_last->data->Status) || $this->_last->data->Status!='OK') return false;
		
		return true;
	}
	
}
