<?php 

namespace FastLabs\Xero\Structures;

Class Phone {
	/** @var string DDI , DEFAULT , FAX , MOBILE */
	public $PhoneType;
	public $PhoneNumber;
	public $PhoneAreaCode;
	public $PhoneCountryCode;
}