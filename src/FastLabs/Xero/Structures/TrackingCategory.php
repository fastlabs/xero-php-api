<?php 

namespace FastLabs\Xero\Structures;

Class TrackingCategory {
	public $TrackingCategoryID;
	
	public $Name;
	
	public $Status;
	
	/** @var TrackingOption[] */
	public $Options;
}



Class TrackingOption {
	public $TrackingOptionID;
	
	public $Name;
	
	public $Status;
}

