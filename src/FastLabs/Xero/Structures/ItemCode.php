<?php 
/**
 * Invoice Items Codes
 */
namespace FastLabs\Xero\Structures;

/**
 * Xero Invoices Items Codes based on
 * https://developer.xero.com/documentation/api/Items
 */
Class ItemCode extends Skeleton {
	
	protected $_actionName = 'Items';
	protected $_GUID = 'ItemID';
	
	/**
	 * Xero generated identifier for an item
	 * @var string uuid
	 */
	public $ItemID;
	
	/**
	 * User defined item code
	 * @var string eg Merino-2011-LG
	 */
	public $Code;
	
	/**
	 * The name of the item	
	 * @var string
	 */
	public $Name;
	
	/**
	 * When IsSold is true the item will be available on sales transactions in the Xero UI.	
	 * @var bool
	 */
	public $IsSold;
	
	/**
	 * When IsPurchased is true the item is available for purchase transactions in the Xero UI.
	 * @var bool
	 */
	public $IsPurchased;
	
	/**
	 * The sales description of the item
	 * @var string
	 */
	public $Description;
	
	/**
	 * The purchase description of the item	
	 * @var string
	 */
	public $PurchaseDescription;
	
	/**
	 * @var ElementsDetails
	 */
	public $PurchaseDetails;
	
	/**
	 * 
	 * @var ElementsDetails
	 */
	public $SalesDetails;
	
	/**
	 * True for items that are tracked as inventory. An item will be tracked as inventory if the InventoryAssetAccountCode and COGSAccountCode are set.
	 * @var bool
	 */
	public $IsTrackedAsInventory;
	
	/**
	 * The inventory asset account for the item. The account must be of type INVENTORY. The COGSAccountCode in PurchaseDetails is also required to create a tracked item
	 * @var string eg 630
	 */
	public $InventoryAssetAccountCode;
	
	/**
	 * The value of the item on hand. Calculated using average cost accounting.	
	 * @var float
	 */
	public $TotalCostPool;
	
	/**
	 * The quantity of the item on hand	
	 * @var float
	 */ 
	public $QuantityOnHand;
	
	/**
	 * Last modified date in UTC format	
	 * @var string eg: /Date(1488338552390+0000)/
	 */
	public $UpdatedDateUTC;
	
}


/**
 * Elements for Purchases and Sales
 */
class ElementsDetails {
	
	/**
	 * Unit Price of the item. By default UnitPrice is returned to two decimal places.
	 * You can use 4 decimal places by adding the unitdp=4 querystring parameter to your request.	
	 * @var float
	 */
	public $UnitPrice;
	
	/**
	 * Default account code to be used for purchased/sale.
	 * Not applicable to the purchase details of tracked items	
	 * @var string eg: 400
	 */
	public $AccountCode;
	
	/**
	 * Cost of goods sold account. Only applicable to the purchase details of tracked items.	
	 * @var string eg 430
	 */
	public $COGSAccountCode;
	
	/**
	 * Used as an override if the default Tax Code for the selected AccountCode is not correct
	 *  - see TaxTypes: https://developer.xero.com/documentation/api/types#TaxTypes
	 * @var string
	 */
	public $TaxType;
	
}