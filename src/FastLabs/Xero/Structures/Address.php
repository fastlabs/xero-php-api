<?php 

namespace FastLabs\Xero\Structures;

Class Address {
	/** @var string eg: STREET */
	public $AddressType;
	public $AddressLine1;
	public $AddressLine2;
	public $AddressLine3;
	public $AddressLine4;
	public $City;
	public $Region;
	public $PostalCode;
	public $Country;
	public $AttentionTo;
}