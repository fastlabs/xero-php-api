<?php 

namespace FastLabs\Xero\Structures;

Class Contact extends Skeleton {
	
	public $ContactID;
	
	/** @var string ACTIVE ARCHIVED GDPRREQUEST */
	public $ContactStatus;
	public $Name;
	
	public $FirstName;
	public $LastName;
	public $EmailAddress;
	public $BankAccountDetails;
	
	public $ContactNumber;
	
	/** @var Address[] */
	public $Addresses;
	
	/** @var Phone[] */
	public $Phones;
	
	/** @var string => /Date(1487615056533+0000)/ */
	public $UpdatedDateUTC;
	
	/** @var array */
	public $ContactGroups;
	
	/** @var bool */
	public $IsSupplier;
	
	/** @var bool */
	public $IsCustomer;
	
	/** @var string eg: NZD */
	public $DefaultCurrency;
	
	/** @var array */
	public $ContactPersons;
	
	protected $_actionName = 'Contacts';
	protected $_GUID       = 'ContactID';
	
}