<?php 

namespace FastLabs\Xero\Structures;

Class Payment extends Skeleton {
	
	/** @var string uuid */
	public $PaymentID;
	
	/** @var string /Date(1455667200000+0000)/ */
	public $Date;
	
	/** @var float */
	public $BankAmount;
	
	/** @var float */
	public $Amount;
	
	/** @var string INV-001 */
	public $Reference;
	
	/** @var float */
	public $CurrencyRate;
	
	/** @var string eg: ACCRECPAYMENT */
	public $PaymentType;
	
	/** @var string eg: AUTHORISED */
	public $Status;
	
	/** @var string /Date(1455667200000+0000)/ */
	public $UpdatedDateUTC;
	
	/** @var bool */
	public $HasAccount;
	
	/** @var bool */
	public $IsReconciled;
	
	/** @var PaymentAccount */
	public $Account;
	
	/** @var PaymentInvoice */
	public $Invoice;
	
	/** @var PaymentInvoiceContact */
	public $Contact;
	
	protected $_actionName = 'Payments';
	protected $_GUID = 'PaymentID';
}

class PaymentAccount {
	public $AccountID;
	public $Code;
}


class PaymentInvoice {
	/** @var string ACCREC */
	public $Type = 'ACCREC';
	/** @var string uuid */
	public $InvoiceID;
	/** @var string INV-001 */
	public $InvoiceNumber;
}

class PaymentInvoiceContact {
	public $ContactID;
	public $Name;
}