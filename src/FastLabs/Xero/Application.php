<?php

namespace FastLabs\Xero;

class Application extends StructuresLoader {
	
	public static $TYPE = 'Unknown';
	
	public static $XeroAccount;
	public static $XeroBranding;
	
	public static $XeroConsumerKey;
	public static $XeroSharedSecret;
	
	public static $RSAprivateKey;
	public static $RSApublicKey;
	
	public static $OauthCallBack;
	
	/** @var \FastLabs\Xero\Application */
	private static $_self;
	
	private function __construct( $config ) {
		parent::__construct( $config );
	}
	
	/**
	 * Start Private Application Interface
	 * @throws \Exception
	 * @return \FastLabs\Xero\Application
	 */
	public static function Private() {
		
		if (self::$TYPE!='Unknown' && self::$TYPE!='Private')
			throw new \Exception("Already Loaded as ".self::$TYPE);
			
		if (self::$XeroConsumerKey=='' || self::$XeroSharedSecret=='' || self::$RSAprivateKey=='' ||  self::$RSApublicKey=='')
			throw new \Exception("Please check the configuration, can't start a Private Application.");
			
		self::$TYPE = 'Private';
		
		if (self::$_self == null) {
			
			$config = [
					'consumer_key'        => self::$XeroConsumerKey,
					'shared_secret'       => self::$XeroSharedSecret,
					'oauth_token'         => self::$XeroConsumerKey,
					'access_token_secret' => self::$XeroSharedSecret,
					'core_version'        => '2.0',
					'payroll_version'     => '1.0',
					'file_version'        => '1.0',
					'rsa_private_key'     => self::$RSAprivateKey,
					'rsa_public_key'      => self::$RSApublicKey,
					'application_type'    => 'Private',
					'oauth_callback'      => 'oob',
					'user_agent'          => 'FastLabs Private Xero App\2.1'
			];
			$class = get_called_class();
			self::$_self = new $class( $config );
			
			self::$_self->checkSSLcerts();
		}
		
		return self::$_self;
		
	}
	
	/**
	 * Start Public Application Interface
	 * @throws \Exception
	 * @return \FastLabs\Xero\Application
	 */
	public static function Public() {
		
		if (self::$TYPE!='Unknown' && self::$TYPE!='Public')
			throw new \Exception("Already Loaded as ".self::$TYPE);
			
		if (self::$XeroConsumerKey=='' || self::$XeroSharedSecret=='' || self::$OauthCallBack=='')
			throw new \Exception("Please check the configuration, can't start a Public Application.");
		
		self::$TYPE = 'Public';
		
		if (self::$_self == null) {
			
			if (session_status() == PHP_SESSION_NONE) session_start();
			
			$config = [
					'consumer_key'    => self::$XeroConsumerKey,
					'shared_secret'   => self::$XeroSharedSecret,
					'core_version'    => '2.0',
					'payroll_version' => '1.0',
					'file_version'    => '1.0',
					'application_type'=> 'Public',
					'oauth_callback'  => self::$OauthCallBack,
					'user_agent'      => 'FastLabs Private Xero App\2.1', //'FastLabs Private Xero App\2.1'
			];
			
			$class = get_called_class();
			self::$_self = new $class( $config );
			
			if (isset($_SESSION['access_token'])) {
				self::$_self->config['access_token']        = $_SESSION['access_token'];
				self::$_self->config['access_token_secret'] = $_SESSION['oauth_token_secret'];
			}
		}
		
		return self::$_self;	
	}
	
	
	public function checkCallBack() {
		if (!isset($_REQUEST['oauth_token'])) return false;
		$this->config['access_token'] = $_SESSION ['oauth'] ['oauth_token'];
		$this->config['access_token_secret'] = $_SESSION ['oauth'] ['oauth_token_secret'];
		
		$this->request ( 'GET', $this->url ( 'AccessToken', '' ), array (
				'oauth_verifier' => $_REQUEST ['oauth_verifier'],
				'oauth_token' => $_REQUEST ['oauth_token']
		) );
		
		if ($this->response['code'] == 200) {
			$response = $this->extract_params ( $this->response['response'] );
			\FastLabs\Xero\Application::persistSession( $response );
			unset ( $_SESSION ['oauth'] );
			header("Location: /");
			exit;
		}
		return header("Location: /?error=".$this->response['response']);
	}
	
	public function getLoginUrl() {
		$params = array ( 'oauth_callback' => \FastLabs\Xero\Application::$OauthCallBack );
		$this->request ( 'GET', $this->url ( 'RequestToken', '' ), $params );
		if ($this->response ['code'] == 200) {
			$_SESSION ['oauth'] = $this->extract_params ( $this->response ['response'] );
			return $this->url ( "Authorize", '' ) . "?oauth_token={$_SESSION['oauth']['oauth_token']}&scope=";
		} else {
			return '#Something not right... sorry!';
		}
	}
	
	public function isLogged() {
		return isset($_SESSION['access_token']);
	}
	
	/**
	 * Persist the OAuth access token and session handle somewhere
	 * In my example I am just using the session, but in real world, this is should be a storage engine
	 *
	 * @param array $params the response parameters as an array of key=value pairs
	 */
	public static function persistSession($response) {
		if (!isset($response)) return false;
		$_SESSION['access_token']       = $response['oauth_token'];
		$_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];
		if(isset($response['oauth_session_handle']))  $_SESSION['session_handle']     = $response['oauth_session_handle'];
	}
	
}
